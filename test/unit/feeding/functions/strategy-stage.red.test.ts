import { feddingExcelToData } from '../../../../src/index';
import { BadRequestException } from '@nestjs/common';
import { Excel } from '../../../../src/lib/feeding/interfaces/excel.interface';
import * as XLSX from 'xlsx';
import { Express } from 'express';

const createMockFile = (data: any): Express.Multer.File => {
  const workbook = XLSX.utils.book_new();
  const sheet = XLSX.utils.json_to_sheet(data);
  XLSX.utils.book_append_sheet(workbook, sheet, 'Sheet1');
  const buffer = XLSX.write(workbook, { type: 'buffer', bookType: 'xlsx' });

  return {
    buffer: buffer,
    originalname: 'test-file.xlsx',
    encoding: 'utf-8',
    mimetype: 'application/vnd.ms-excel',
    size: 1024,
    destination: './uploads',
    filename: 'test-file.xlsx',
    path: './uploads/test-file.xlsx',
    fieldname: 'file',
    bufferLength: 1024,
  };
};

describe('feddingExcelToData', () => {
  it('should convert Excel data to DTOs', () => {
    const excelData: Excel[] = [
      { Peso: 100, Biomasa: 50, Tipo: 'foo / bar', Horas: '1 / 2' },
      { Peso: 200, Biomasa: 75, Tipo: 'baz / qux', Horas: '3 / 4' },
    ];

    const mockFile = createMockFile(excelData);

    const expectedOutput = [
      {
        targetSizeInGrams: 100,
        percentageOfBiomass: 50,
        characteristics: ['foo', 'bar'],
        times: ['1', '2'],
      },
      {
        targetSizeInGrams: 200,
        percentageOfBiomass: 75,
        characteristics: ['baz', 'qux'],
        times: ['3', '4'],
      },
    ];

    const actualOutput = feddingExcelToData(mockFile);

    expect(actualOutput).toEqual(expectedOutput);
  });
});
