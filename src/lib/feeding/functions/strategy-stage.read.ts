import { BadRequestException } from '@nestjs/common';
import { Express } from 'express';
import * as XLSX from 'xlsx';
import { Excel } from '../interfaces/excel.interface';

const changeToArray = (iteam: string): string[] => iteam.split(' / ');

const createNewStrategyStage = <Dto>(excelData: Excel): Dto | any => ({
  targetSizeInGrams: excelData.Peso,

  percentageOfBiomass: excelData.Biomasa,

  characteristics: changeToArray(excelData.Tipo),

  times: changeToArray(excelData.Horas),
});

export const feddingExcelToData = <Dto>(file: Express.Multer.File): Dto[] => {
  const workbook = XLSX.read(file.buffer);

  const workbookSheets = workbook.SheetNames;

  const excelData: Excel[] = [].concat(
    ...workbookSheets.map((sheet) => XLSX.utils.sheet_to_json(workbook.Sheets[sheet])),
  );

  const newData = excelData.map((i) => createNewStrategyStage<Dto>(i));

  return newData;
};
