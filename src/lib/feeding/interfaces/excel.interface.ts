export interface Excel {
  Peso: number;

  Biomasa: number;

  Tipo: string;

  Horas: string;
}
